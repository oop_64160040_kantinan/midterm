package com.midterm;

public class App {
    public static void main(String[] args) {
        Character Character1 = new Character("Hutao", "Female", "Liyue", 90, "DPS");
        // ทำการกำหนดค่า constructor ใน Class ของ Character ตั้งชื่อเป็น Character1 
        // และสร้าง constructor ใหม่  และป้อน ค่าที่ต้องการใส่ใน object ต่างๆที่อยู่ในวงเล็บ 
        // เช่น ตัวแรกคือ object name ใส่ค่า Hutao ไป name โปรแกรมก็จะดำเนินการใน Class ของ Character 
        // บรรทัดที่ 25 เเละ name ก็จะกลายเป็นค่า Hutao โค้ดคือ System.out.println("Name : " + name);
        // บรรทัดที่ 26-29 คือการรับค่าจาก constructor ใหม่ และเก็บลงไปใน object  ถัดๆไปของโปรเเกรม โดยสุดท้ายคือ object position
        Skill skillCharacter1 = new Skill("Paramita Papilio", "Spirit Soother", 10, 9);
        // ทำการกำหนดค่า constructor ใน Class ของ Skill ตั้งชื่อเป็น skillCharacter1 
        // และสร้าง constructor ใหม่ และป้อน ค่าที่ต้องการใส่ใน object ต่างๆที่อยู่ในวงเล็บ 
        // เช่น ตัวแรกคือ object elementalSkill ใส่ค่า Paramita Papilio ไป elementalSkill โปรแกรมก็จะดำเนินการใน Class ของ Skill 
        // บรรทัดที่ 23 เเละ elementalSkill ก็จะกลายเป็นค่า Paramita Papilio โค้ดคือ System.out.println("NameElementalSkill : " + elementalSkill);
        // บรรทัดที่ 24-26 คือการรับค่าจาก constructor ใหม่ และเก็บลงไปใน object  ถัดๆไปของโปรเเกรม โดยสุดท้ายคือ object levelUltimateSkill
        Character1.printCharacter();
        // กระบวนการเรียกใช้ method printCharacter() จาก Class ของ Character เพื่อเป็นการแสดงผลออกทางหน้าจอ 
        // โดยจะแสดงออกมาในรูปแบบของตัวอักษรตัวเลข ที่ได้รับค่า object มาจาก constructor ใหม่ที่ถูกสร้างขึ้นก่อนหน้านี้ที่ชื่อ Character1 
        // โดยการให้ constructor Character1 เป็นตัวใช้ method printCharacter() เพื่อที่จะแสดงผลตอบมาเป็นข้อมูลที่ต้องการ 
        skillCharacter1.printSkill();
        // กระบวนการเรียกใช้ method printSkill() จาก Class ของ Skill เพื่อเป็นการแสดงผลออกทางหน้าจอ 
        // โดยจะแสดงออกมาในรูปแบบของตัวอักษรตัวเลข ที่ได้รับค่า object มาจาก constructor ใหม่ที่ถูกสร้างขึ้นก่อนหน้านี้ที่ชื่อ skillCharacter1
        // โดยการให้ constructor skillCharacter1 เป็นตัวใช้ method printSkill() เพื่อที่จะแสดงผลตอบมาเป็นข้อมูลที่ต้องการ 
        Character Character2 = new Character("Xingqiu", "Male", "Liyue", 80, "SubDPS");
        // ทำการกำหนดค่า constructor ใน Class ของ Character ตั้งชื่อเป็น Character2
        // และสร้าง constructor ใหม่  และป้อน ค่าที่ต้องการใส่ใน object ต่างๆที่อยู่ในวงเล็บ 
        // เช่น ตัวแรกคือ object name ใส่ค่า Xingqiu ไป name โปรแกรมก็จะดำเนินการใน Class ของ Character 
        // บรรทัดที่ 25 เเละ name ก็จะกลายเป็นค่า Xingqiu โค้ดคือ System.out.println("Name : " + name);
        // บรรทัดที่ 26-29 คือการรับค่าจาก constructor ใหม่ และเก็บลงไปใน object  ถัดๆไปของโปรเเกรม โดยสุดท้ายคือ object position
        Skill skillCharacter2 = new Skill("Fatal Rainscreen", "Riancutter", 8, 10);
        // ทำการกำหนดค่า constructor ใน Class ของ Skill ตั้งชื่อเป็น skillCharacter2 
        // และสร้าง constructor ใหม่ และป้อน ค่าที่ต้องการใส่ใน object ต่างๆที่อยู่ในวงเล็บ 
        // เช่น ตัวแรกคือ object elementalSkill ใส่ค่า Fatal Rainscreen ไป elementalSkill โปรแกรมก็จะดำเนินการใน Class ของ Skill 
        // บรรทัดที่ 23 เเละ elementalSkill ก็จะกลายเป็นค่า Fatal Rainscreen โค้ดคือ System.out.println("NameElementalSkill : " + elementalSkill);
        // บรรทัดที่ 24-26 คือการรับค่าจาก constructor ใหม่ และเก็บลงไปใน object  ถัดๆไปของโปรเเกรม โดยสุดท้ายคือ object levelUltimateSkill
        Character2.printCharacter();
        // กระบวนการเรียกใช้ method printCharacter() จาก Class ของ Character เพื่อเป็นการแสดงผลออกทางหน้าจอ 
        // โดยจะแสดงออกมาในรูปแบบของตัวอักษรตัวเลข ที่ได้รับค่า object มาจาก constructor ใหม่ที่ถูกสร้างขึ้นก่อนหน้านี้ที่ชื่อ Character2
        // โดยการให้ constructor Character2 เป็นตัวใช้ method printCharacter() เพื่อที่จะแสดงผลตอบมาเป็นข้อมูลที่ต้องการ
        skillCharacter2.printSkill();
        // กระบวนการเรียกใช้ method printSkill() จาก Class ของ Skill เพื่อเป็นการแสดงผลออกทางหน้าจอ 
        // โดยจะแสดงออกมาในรูปแบบของตัวอักษรตัวเลข ที่ได้รับค่า object มาจาก constructor ใหม่ที่ถูกสร้างขึ้นก่อนหน้านี้ที่ชื่อ skillCharacter2
        // โดยการให้ constructor skillCharacter2 เป็นตัวใช้ method printSkill() เพื่อที่จะแสดงผลตอบมาเป็นข้อมูลที่ต้องการ
        Character Character3 = new Character("Bennett", "Male", "Monstadt", 80, "Support");
        // ทำการกำหนดค่า constructor ใน Class ของ Character ตั้งชื่อเป็น Character3
        // และสร้าง constructor ใหม่  และป้อน ค่าที่ต้องการใส่ใน object ต่างๆที่อยู่ในวงเล็บ 
        // เช่น ตัวแรกคือ object name ใส่ค่า Bennett ไป name โปรแกรมก็จะดำเนินการใน Class ของ Character 
        // บรรทัดที่ 25 เเละ name ก็จะกลายเป็นค่า Bennett โค้ดคือ System.out.println("Name : " + name);
        // บรรทัดที่ 26-29 คือการรับค่าจาก constructor ใหม่ และเก็บลงไปใน object  ถัดๆไปของโปรเเกรม โดยสุดท้ายคือ object position
        Skill skillCharacter3 = new Skill("Passion Overload", "Inspiration Field", 6, 8);
        // ทำการกำหนดค่า constructor ใน Class ของ Skill ตั้งชื่อเป็น skillCharacter3
        // และสร้าง constructor ใหม่ และป้อน ค่าที่ต้องการใส่ใน object ต่างๆที่อยู่ในวงเล็บ 
        // เช่น ตัวแรกคือ object elementalSkill ใส่ค่า Passion Overload ไป elementalSkill โปรแกรมก็จะดำเนินการใน Class ของ Skill 
        // บรรทัดที่ 23 เเละ elementalSkill ก็จะกลายเป็นค่า Passion Overload โค้ดคือ System.out.println("NameElementalSkill : " + elementalSkill);
        // บรรทัดที่ 24-26 คือการรับค่าจาก constructor ใหม่ และเก็บลงไปใน object  ถัดๆไปของโปรเเกรม โดยสุดท้ายคือ object levelUltimateSkill
        Character3.printCharacter();
        // กระบวนการเรียกใช้ method printCharacter() จาก Class ของ Character เพื่อเป็นการแสดงผลออกทางหน้าจอ 
        // โดยจะแสดงออกมาในรูปแบบของตัวอักษรตัวเลข ที่ได้รับค่า object มาจาก constructor ใหม่ที่ถูกสร้างขึ้นก่อนหน้านี้ที่ชื่อ Character3
        // โดยการให้ constructor Character3 เป็นตัวใช้ method printCharacter() เพื่อที่จะแสดงผลตอบมาเป็นข้อมูลที่ต้องการ
        skillCharacter3.printSkill();
        // กระบวนการเรียกใช้ method printSkill() จาก Class ของ Skill เพื่อเป็นการแสดงผลออกทางหน้าจอ 
        // โดยจะแสดงออกมาในรูปแบบของตัวอักษรตัวเลข ที่ได้รับค่า object มาจาก constructor ใหม่ที่ถูกสร้างขึ้นก่อนหน้านี้ที่ชื่อ skillCharacter3
        // โดยการให้ constructor skillCharacter3 เป็นตัวใช้ method printSkill() เพื่อที่จะแสดงผลตอบมาเป็นข้อมูลที่ต้องการ
        Character Character4 = new Character("Xingling", "Female", "Liyue", 80, "SubDPS");
        // ทำการกำหนดค่า constructor ใน Class ของ Character ตั้งชื่อเป็น Character4
        // และสร้าง constructor ใหม่  และป้อน ค่าที่ต้องการใส่ใน object ต่างๆที่อยู่ในวงเล็บ 
        // เช่น ตัวแรกคือ object name ใส่ค่า Xingling ไป name โปรแกรมก็จะดำเนินการใน Class ของ Character 
        // บรรทัดที่ 25 เเละ name ก็จะกลายเป็นค่า Xingling โค้ดคือ System.out.println("Name : " + name);
        // บรรทัดที่ 26-29 คือการรับค่าจาก constructor ใหม่ และเก็บลงไปใน object  ถัดๆไปของโปรเเกรม โดยสุดท้ายคือ object position
        Skill skillCharacter4 = new Skill("Guoba Attack", "Pyronado", 5, 8);
        // ทำการกำหนดค่า constructor ใน Class ของ Skill ตั้งชื่อเป็น skillCharacter4
        // และสร้าง constructor ใหม่ และป้อน ค่าที่ต้องการใส่ใน object ต่างๆที่อยู่ในวงเล็บ 
        // เช่น ตัวแรกคือ object elementalSkill ใส่ค่า Guoba Attack ไป elementalSkill โปรแกรมก็จะดำเนินการใน Class ของ Skill 
        // บรรทัดที่ 23 เเละ elementalSkill ก็จะกลายเป็นค่า Guoba Attack โค้ดคือ System.out.println("NameElementalSkill : " + elementalSkill);
        // บรรทัดที่ 24-26 คือการรับค่าจาก constructor ใหม่ และเก็บลงไปใน object  ถัดๆไปของโปรเเกรม โดยสุดท้ายคือ object levelUltimateSkill
        Character4.printCharacter();
        // กระบวนการเรียกใช้ method printCharacter() จาก Class ของ Character เพื่อเป็นการแสดงผลออกทางหน้าจอ 
        // โดยจะแสดงออกมาในรูปแบบของตัวอักษรตัวเลข ที่ได้รับค่า object มาจาก constructor ใหม่ที่ถูกสร้างขึ้นก่อนหน้านี้ที่ชื่อ Character4
        // โดยการให้ constructor Character4 เป็นตัวใช้ method printCharacter() เพื่อที่จะแสดงผลตอบมาเป็นข้อมูลที่ต้องการ
        skillCharacter4.printSkill();
        // กระบวนการเรียกใช้ method printSkill() จาก Class ของ Skill เพื่อเป็นการแสดงผลออกทางหน้าจอ 
        // โดยจะแสดงออกมาในรูปแบบของตัวอักษรตัวเลข ที่ได้รับค่า object มาจาก constructor ใหม่ที่ถูกสร้างขึ้นก่อนหน้านี้ที่ชื่อ skillCharacter4
        // โดยการให้ constructor skillCharacter4 เป็นตัวใช้ method printSkill() เพื่อที่จะแสดงผลตอบมาเป็นข้อมูลที่ต้องการ
        Character Character5 = new Character("Zhongli", "Male", "Liyue", 90, "Support");
        // ทำการกำหนดค่า constructor ใน Class ของ Character ตั้งชื่อเป็น Character5
        // และสร้าง constructor ใหม่  และป้อน ค่าที่ต้องการใส่ใน object ต่างๆที่อยู่ในวงเล็บ 
        // เช่น ตัวแรกคือ object name ใส่ค่า Zhongli ไป name โปรแกรมก็จะดำเนินการใน Class ของ Character 
        // บรรทัดที่ 25 เเละ name ก็จะกลายเป็นค่า Zhongli โค้ดคือ System.out.println("Name : " + name);
        // บรรทัดที่ 26-29 คือการรับค่าจาก constructor ใหม่ และเก็บลงไปใน object  ถัดๆไปของโปรเเกรม โดยสุดท้ายคือ object position
        Skill skillCharacter5 = new Skill("Dominus Lapidis", "Planet Befall", 9, 4);
        // ทำการกำหนดค่า constructor ใน Class ของ Skill ตั้งชื่อเป็น skillCharacter5
        // และสร้าง constructor ใหม่ และป้อน ค่าที่ต้องการใส่ใน object ต่างๆที่อยู่ในวงเล็บ 
        // เช่น ตัวแรกคือ object elementalSkill ใส่ค่า Dominus Lapidis ไป elementalSkill โปรแกรมก็จะดำเนินการใน Class ของ Skill 
        // บรรทัดที่ 23 เเละ elementalSkill ก็จะกลายเป็นค่า Dominus Lapidis โค้ดคือ System.out.println("NameElementalSkill : " + elementalSkill);
        // บรรทัดที่ 24-26 คือการรับค่าจาก constructor ใหม่ และเก็บลงไปใน object  ถัดๆไปของโปรเเกรม โดยสุดท้ายคือ object levelUltimateSkill
        Character5.printCharacter();
        // กระบวนการเรียกใช้ method printCharacter() จาก Class ของ Character เพื่อเป็นการแสดงผลออกทางหน้าจอ 
        // โดยจะแสดงออกมาในรูปแบบของตัวอักษรตัวเลข ที่ได้รับค่า object มาจาก constructor ใหม่ที่ถูกสร้างขึ้นก่อนหน้านี้ที่ชื่อ Character5
        // โดยการให้ constructor Character5 เป็นตัวใช้ method printCharacter() เพื่อที่จะแสดงผลตอบมาเป็นข้อมูลที่ต้องการ
        skillCharacter5.printSkill();
        // กระบวนการเรียกใช้ method printSkill() จาก Class ของ Skill เพื่อเป็นการแสดงผลออกทางหน้าจอ 
        // โดยจะแสดงออกมาในรูปแบบของตัวอักษรตัวเลข ที่ได้รับค่า object มาจาก constructor ใหม่ที่ถูกสร้างขึ้นก่อนหน้านี้ที่ชื่อ skillCharacter5
        // โดยการให้ constructor skillCharacter5 เป็นตัวใช้ method printSkill() เพื่อที่จะแสดงผลตอบมาเป็นข้อมูลที่ต้องการ
        Character Character6 = new Character("Sangonomiya Kokomi", "Female", "Inazuma", 80, "Support");
        // ทำการกำหนดค่า constructor ใน Class ของ Character ตั้งชื่อเป็น Character6
        // และสร้าง constructor ใหม่  และป้อน ค่าที่ต้องการใส่ใน object ต่างๆที่อยู่ในวงเล็บ 
        // เช่น ตัวแรกคือ object name ใส่ค่า Sangonomiya Kokomi ไป name โปรแกรมก็จะดำเนินการใน Class ของ Character 
        // บรรทัดที่ 25 เเละ name ก็จะกลายเป็นค่า Sangonomiya Kokomi โค้ดคือ System.out.println("Name : " + name);
        // บรรทัดที่ 26-29 คือการรับค่าจาก constructor ใหม่ และเก็บลงไปใน object  ถัดๆไปของโปรเเกรม โดยสุดท้ายคือ object position
        Skill skillCharacter6 = new Skill("Kurage'Oath", "Nereid'Ascension", 9, 8);
        // ทำการกำหนดค่า constructor ใน Class ของ Skill ตั้งชื่อเป็น skillCharacter6
        // และสร้าง constructor ใหม่ และป้อน ค่าที่ต้องการใส่ใน object ต่างๆที่อยู่ในวงเล็บ 
        // เช่น ตัวแรกคือ object elementalSkill ใส่ค่า Kurage'Oath ไป elementalSkill โปรแกรมก็จะดำเนินการใน Class ของ Skill 
        // บรรทัดที่ 23 เเละ elementalSkill ก็จะกลายเป็นค่า Kurage'Oath โค้ดคือ System.out.println("NameElementalSkill : " + elementalSkill);
        // บรรทัดที่ 24-26 คือการรับค่าจาก constructor ใหม่ และเก็บลงไปใน object  ถัดๆไปของโปรเเกรม โดยสุดท้ายคือ object levelUltimateSkill
        Character6.printCharacter();
        // กระบวนการเรียกใช้ method printCharacter() จาก Class ของ Character เพื่อเป็นการแสดงผลออกทางหน้าจอ 
        // โดยจะแสดงออกมาในรูปแบบของตัวอักษรตัวเลข ที่ได้รับค่า object มาจาก constructor ใหม่ที่ถูกสร้างขึ้นก่อนหน้านี้ที่ชื่อ Character6
        // โดยการให้ constructor Character6 เป็นตัวใช้ method printCharacter() เพื่อที่จะแสดงผลตอบมาเป็นข้อมูลที่ต้องการ
        skillCharacter6.printSkill();
        // กระบวนการเรียกใช้ method printSkill() จาก Class ของ Skill เพื่อเป็นการแสดงผลออกทางหน้าจอ 
        // โดยจะแสดงออกมาในรูปแบบของตัวอักษรตัวเลข ที่ได้รับค่า object มาจาก constructor ใหม่ที่ถูกสร้างขึ้นก่อนหน้านี้ที่ชื่อ skillCharacter6
        // โดยการให้ constructor skillCharacter6 เป็นตัวใช้ method printSkill() เพื่อที่จะแสดงผลตอบมาเป็นข้อมูลที่ต้องการ

    }
}