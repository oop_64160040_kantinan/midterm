package com.midterm;

public class Character {
    private String name;
    private String sex;
    private String city;
    private double level;
    private String position;

    // บรรทัดที่ 4-8 เป็นการสร้าง object ขึ้นมาใหม่ 4 ตัวเพื่อรอรับค่า
    public Character(String name, String sex, String city, double level, String position) {
        // บรรทัดที่ 11 สร้าง constructor Character() และใส่ object ในวงเล็บ
        this.name = name;
        this.sex = sex;
        this.city = city;
        this.level = level;
        this.position = position;
        // บรรทัดที่ 13-17 ทำการ this. เพื่อให้รู้ว่า Object ตัวนี้เท่ากับ Object ตัวนี้
    }

    public void printCharacter() {
        System.out.println("=========================================");
        System.out.println("               Character");
        System.out.println("=========================================");
        System.out.println("Name : " + name);
        System.out.println("Sex : " + sex);
        System.out.println("City : " + city);
        System.out.println("Level : " + level + " Level ");
        System.out.println("Position : " + position);
        // บรรทัดที่ 22-29 ทำการสร้าง method printCharacter() ขึ้นมาโดย method
        // นี้จะทำการแสดงผลข้อมูลที่ได้พิมพ์ไว้
        // และการเติมค่าของ object เข้าไปใน object ของตัวนั้นๆ เพื่อแสดงค่า
    }

    public String getName() {
        return name;
    }

    // บรรทัดที่ 35-36 ทำการ get เพื่อให้ค่า object ที่เป็น private สามารถใช้ได้ ของ
    // object name และ return ค่ากลับไปใน object นั้น
    public String getSex() {
        return sex;
    }

    // บรรทัดที่ 41-42 ทำการ get เพื่อให้ค่า object ที่เป็น private สามารถใช้ได้ ของ
    // object sex และ return ค่ากลับไปใน object นั้น
    public String getCity() {
        return city;
    }

    // บรรทัดที่ 47-48 ทำการ get เพื่อให้ค่า object ที่เป็น private สามารถใช้ได้ ของ
    // object city และ return ค่ากลับไปใน object นั้น
    public double getLevel() {
        return level;
    }

    // บรรทัดที่ 53-54 ทำการ get เพื่อให้ค่า object ที่เป็น private สามารถใช้ได้ ของ
    // object level และ return ค่ากลับไปใน object นั้น
    public String getPosition() {
        return position;
    }
    // บรรทัดที่ 59-60 ทำการ get เพื่อให้ค่า object ที่เป็น private สามารถใช้ได้ ของ
    // object position และ return ค่ากลับไปใน object นั้น
}
