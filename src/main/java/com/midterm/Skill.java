package com.midterm;

public class Skill {
    private String elementalSkill;
    private String ultimateSkill;
    private double levelElementalSkill;
    private double levelUltimateSkill;

    // บรรทัดที่ 4-7 เป็นการสร้าง object ขึ้นมาใหม่ 4 ตัวเพื่อรอรับค่า
    public Skill(String elementalSkill, String ultimateSkill, double levelElementalSkill, double levelUltimateSkill) {
        // บรรทัดที่ 10 สร้าง constructor Skill() และใส่ object ในวงเล็บ
        this.elementalSkill = elementalSkill;
        this.ultimateSkill = ultimateSkill;
        this.levelElementalSkill = levelElementalSkill;
        this.levelUltimateSkill = levelUltimateSkill;
        // บรรทัดที่ 12-15 ทำการ this. เพื่อให้รู้ว่า Object ตัวนี้เท่ากับ Object ตัวนี้
    }

    public void printSkill() {
        System.out.println("=========================================");
        System.out.println("                 Skill");
        System.out.println("=========================================");
        System.out.println("NameElementalSkill : " + elementalSkill);
        System.out.println("NameUltimateSkill : " + ultimateSkill);
        System.out.println("LevelElementalSkil : " + levelElementalSkill + " Level ");
        System.out.println("LevelUltimateSkill : " + levelUltimateSkill + " Level ");
        System.out.println("=========================================");
        // บรรทัดที่ 20-27 ทำการสร้าง method printSkill() ขึ้นมาโดย method
        // นี้จะทำการแสดงผลข้อมูลที่ได้พิมพ์ไว้
        // และการเติมค่าของ object เข้าไปใน object ของตัวนั้นๆ เพื่อแสดงค่า
    }

    public String getElementalSkill() {
        return elementalSkill;
    }

    // บรรทัดที่ 33-34 ทำการ get เพื่อให้ค่า object ที่เป็น private สามารถใช้ได้ ของ
    // object elementalSkill และ return ค่ากลับไปใน object นั้น
    public String getUltimateSkill() {
        return ultimateSkill;
    }

    // บรรทัดที่ 39-40 ทำการ get เพื่อให้ค่า object ที่เป็น private สามารถใช้ได้ ของ
    // object ultimateSkill และ return ค่ากลับไปใน object นั้น
    public double getLevelElementalSkill() {
        return levelElementalSkill;
    }

    // บรรทัดที่ 45-46 ทำการ get เพื่อให้ค่า object ที่เป็น private สามารถใช้ได้ ของ
    // object levelElementalSkill และ return ค่ากลับไปใน object นั้น
    public double getLevelUltimateSkill() {
        return levelUltimateSkill;
    }
    // บรรทัดที่ 51-52 ทำการ get เพื่อให้ค่า object ที่เป็น private สามารถใช้ได้ ของ
    // object levelUltimateSkill และ return ค่ากลับไปใน object นั้น
}
